#include "Tracer.h"
#include <iostream>

bool raySphereIntersection(SRay ray, vec3 pred_pos, SBlackHole bh, vec3& point)
{
    vec3 tmp = ray.m_start - pred_pos;
    vec3 k = pred_pos - bh.m_center;
    vec3 ray_dir = normalize(tmp);
    float b = dot(k, ray_dir);
    float c = dot(k, k) - bh.m_rad*bh.m_rad;
    float d = b*b - c;
    if(d >= 0) {
        float sqrtfd = sqrtf(d);
        float t1 = -b + sqrtfd;
        float t2 = -b - sqrtfd;
        float min_t  = glm::min(t1,t2);
        float max_t = glm::max(t1,t2);
        float t = (min_t >= 0) ? min_t : max_t;
        point.x = pred_pos.x + ray_dir.x * t;
        point.y = pred_pos.y + ray_dir.y * t;
        point.z = pred_pos.z + ray_dir.z * t;
        if(length(point - pred_pos) > length(ray.m_start - pred_pos)) return false;
        //point = normalize(point)*(float)bh.m_rad;
        return (t > 0);
    }
    return false;
}

bool rayDiskIntersection(SRay ray, vec3 pred_pos, SDisk disk, SBlackHole bh, vec2& point)
{
    vec3 tmp = ray.m_start - pred_pos;
    tmp = normalize(tmp);
    if(tmp.z == 0) return false;
    float nx = pred_pos.x-pred_pos.z*tmp.x/tmp.z,
          ny = pred_pos.y-pred_pos.z*tmp.y/tmp.z;
    if(nx*nx + ny*ny > disk.m_rad * disk.m_rad) return false;
    if(nx*nx + ny*ny < bh.m_rad * bh.m_rad)return false;
    point = vec2(nx, ny);
    if(length(vec3(nx, ny, 0) - pred_pos) >  length(ray.m_start - pred_pos)) return false;
    return true;
}

SRay CTracer::MakeRay(glm::uvec2 pixelPos)
{
    SRay ans;
    ans.m_start = m_camera.m_pos;
    ans.m_dir = m_camera.m_forward +
            m_camera.m_right*((0.5f + pixelPos.x)/m_camera.m_resolution.x - 0.5f) +
            m_camera.m_up*((0.5f + pixelPos.y)/m_camera.m_resolution.y - 0.5f);
    return ans;
}

glm::vec3 CTracer::TraceRay(SRay ray)
{
    vec3 pred_pos;
    int k = 0;
    float mass = m_pScene->black_hole.m_mass, q = 0, t_sphere, t_disk;
    vec3 r, a;
    vec2 pt;
    vec3 start_ray;
    vec3 pt1, pt2;
    start_ray = ray.m_dir;
    vec3 cam_pos = ray.m_start;
    ray.m_dir = normalize(ray.m_dir)*3e8f;
    float delta_t = m_pScene->black_hole.m_rad/3/3e8f;
    bool f1 = false, f2 = false;
    while(k < 1e2) {
        //cout << ray.m_start.x << " " << ray.m_start.y << " " << ray.m_start.z << "\n";
        f1 = f2 = false;
        if(k!= 0 && raySphereIntersection(ray, pred_pos, m_pScene->black_hole, pt1)) {
            f1 = true;
            //cout << 1 << endl;
        }
        if(k != 0 && rayDiskIntersection(ray, pred_pos, m_pScene->disk, m_pScene->black_hole, pt)) {
            f2 = true;
            pt2.x = pt.x;
            pt2.y = pt.y;
            pt2.z = 0;
        }
        if(f1 && f2) {
            //cout << length(pt1) << " " << m_pScene->black_hole.m_rad << endl;
            if(length(pt1 - cam_pos) < length(pt2 - cam_pos)) f2 = false;
        }
        if(f2) {
            pt.x = (pt.x + m_pScene->disk.m_rad) / 2 / (m_pScene->disk.m_rad);
            pt.y = (pt.y + m_pScene->disk.m_rad) / 2 / (m_pScene->disk.m_rad);
            int xx = int(pt.x*1024), yy = int(pt.y*1024);
            /*RGBQUAD *col = new RGBQUAD;

            int pit = disk->getScanWidth();
            unsigned char *dat = disk->accessPixels();
            disk->getPixelColor(xx, yy, col);
            float bl = float(dat[yy*pit + xx*3    ]);
            float re = float(dat[yy*pit + xx*3 + 1]);
            float gr = float(dat[yy*pit + xx*3 + 2]);
            return vec3((col->rgbRed, col->rgbGreen, col->rgbBlue)/255.0f);*/
            return vec3(disk[xx][yy]/255.0f);
            /*cout << "\\\\\\\\\\\n";
            std::cin.get();*/

            //return vec3(1,0,0);
        }
        if(f1) {

            /*cout << "\\\\\\\\\\\n";
            std::cin.get();*/
            return vec3(0, 0, 0);
        }
        r = -ray.m_start;
        q = length(r);
        a = (6.674e-11f*mass/q/q/q)*r;
        pred_pos = ray.m_start;
        ray.m_dir += a*delta_t;
        //ray.m_dir = normalize(ray.m_dir)*3e8f;
        ray.m_start += ray.m_dir*delta_t + a*delta_t*delta_t/2.0f;
        k++;
    }
    /*cout << "\\\\\\\\\\\n";
    std::cin.get();*/
    start_ray = normalize(ray.m_dir);
    float phi = atan2(start_ray.x, start_ray.y) + M_PI, theta = asin(start_ray.z)+ M_PI/2;
    int x_cord = int(2048 * phi / 2 / M_PI);
    int y_cord = int(1024 * theta / M_PI);
    //cout << background[x_cord][y_cord].x << " " << background[x_cord][y_cord].y<< endl;
    /*int pitch = background->getScanWidth();
    unsigned char *data = background->accessPixels();
    float blue = float(data[y_cord*pitch + x_cord*3    ]);
    float red = float(data[y_cord*pitch + x_cord*3 + 1]);
    float green = float(data[y_cord*pitch + x_cord*3 + 2]);*/
    return (background[x_cord][y_cord]/255.0f);
    //return vec3(0, 1, 0);
}

void CTracer::RenderImage(int xRes, int yRes)
{
    fipImage* tmp = LoadImageFromFile("data/stars1.png");
    for(int i = 0; i < tmp->getHeight(); i++) {// Image lines
        for(int j = 0; j < tmp->getWidth(); j++) {// Pixels in line
            RGBQUAD *col = new RGBQUAD;
            tmp->getPixelColor(j, i, col);
            /*cout << i << " " << j << endl;*/
            //cout << (float)(col->rgbRed) << " " << (float)(col->rgbGreen) << " " << (float)(col->rgbBlue) << endl;
            background[j][i] = vec3((float)(col->rgbRed), (float)(col->rgbGreen), (float)(col->rgbBlue));
        }
    }
    fipImage* tmp1 = LoadImageFromFile("data/disk_24.png");
    for(int i = 0; i < tmp1->getHeight(); i++) {// Image lines
        for(int j = 0; j < tmp1->getWidth(); j++) {// Pixels in line
            RGBQUAD *col = new RGBQUAD;
            tmp1->getPixelColor(j, i, col);
            //cout << i << " " << j << endl;
            disk[j][i] = vec3(col->rgbRed, col->rgbGreen, col->rgbBlue);
        }
    }
    //cout << background[580][780].x << " " <<background[580][780].y << " " << background[580][780].z << endl;
    // Rendering
    m_camera.m_resolution = uvec2(xRes, yRes);
    m_camera.m_pixels.resize(xRes * yRes);

    for(int i = 0; i < yRes; i++)
        for(int j = 0; j < xRes; j++) {
            SRay ray = MakeRay(uvec2(j, i));
            m_camera.m_pixels[i * xRes + j] = TraceRay(ray);
        }
}

void CTracer::SaveImageToFile(std::string fileName)
{
    fipImage image;

    int width = m_camera.m_resolution.x;
    int height = m_camera.m_resolution.y;

    image.setSize(FIT_BITMAP, width, height, 24);

    int pitch = image.getScanWidth();
    unsigned char* imageBuffer = (unsigned char*)image.accessPixels();

    if (pitch < 0) {
        imageBuffer += pitch * (height - 1);
        pitch = -pitch;
    }

    int i, j;
    int imageDisplacement = 0;
    int textureDisplacement = 0;

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
          vec3 color = m_camera.m_pixels[textureDisplacement + j];

          imageBuffer[imageDisplacement + j * 3] = clamp(color.b, 0.0f, 1.0f) * 255.0f;
          imageBuffer[imageDisplacement + j * 3 + 1] = clamp(color.g, 0.0f, 1.0f) * 255.0f;
          imageBuffer[imageDisplacement + j * 3 + 2] = clamp(color.r, 0.0f, 1.0f) * 255.0f;
        }
        imageDisplacement += pitch;
        textureDisplacement += width;
    }

    image.save(fileName.c_str());
    image.clear();
}

fipImage* CTracer::LoadImageFromFile(std::string fileName)
{
    fipImage *pImage = new fipImage();

    if(pImage->load(fileName.c_str()))
        return pImage;
    else {
        delete pImage;
        return NULL;
    }
}

void CTracer::setCameraPosition(vec2 matrix_res, vec3 pos, int angle)
{
    m_camera.m_pos = pos;
    m_camera.m_resolution = matrix_res;
    m_camera.m_forward = (matrix_res.y / 2.0f / (float)tan(angle/2.0f/180.0f*M_PI)) * normalize(-pos);
    float _x = m_camera.m_forward.x, _y = m_camera.m_forward.y, _z = m_camera.m_forward.z;
    m_camera.m_up = vec3(matrix_res.y * _z / 2.0f / sqrt(_z*_z + _x*_x), 0.0f, -matrix_res.y * _x / 2.0f / sqrt(_z*_z + _x*_x));
    m_camera.m_right = vec3(0.0f, matrix_res.x/2.0f, 0.0f);
}

#include "Tracer.h"
#include "stdio.h"
#include <iostream>

int main(int argc, char** argv)
{
    CTracer tracer;
    CScene scene;

    int xRes, yRes;

    if(argc == 2) {// There is input file in parameters
    FILE* file = fopen(argv[1], "r");
    if(file) {
        int angle;
        double blackHoleMass = 0.0;
        double koef;
        double xCam, yCam, zCam;
        if(fscanf(file, "%d %d %lf %lf %lf %lf %lf %d", &xRes, &yRes, &blackHoleMass,
                &koef, &xCam, &yCam, &zCam, &angle) == 8) {
            scene.init(xRes, yRes, blackHoleMass, koef);
            tracer.setCameraPosition(glm::vec2(xRes, yRes), glm::vec3(xCam, yCam, zCam), angle);
        }
        else
            printf("Invalid config format! Using default parameters.\r\n");

        fclose(file);
    }
    else
        printf("Invalid config path! Using default parameters.\r\n");
    }
    else
        printf("No config! Using default parameters.\r\n");

    tracer.m_pScene = &scene;
    tracer.RenderImage(xRes, yRes);
    tracer.SaveImageToFile("Result.png");
}

#include "Scene.h"

void CScene::init(int xRes, int yRes, double blackHoleMass, double koef)
{
    black_hole.m_center = glm::vec3(0, 0, 0);
    black_hole.m_mass = blackHoleMass;
    black_hole.m_rad = 2*G*black_hole.m_mass/c/c;

    disk.m_rad = black_hole.m_rad * koef;
}

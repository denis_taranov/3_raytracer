#pragma once

#include "glm.hpp"
#include "vector"

const double c = 3.0e8, G = 6.674e-11;

struct SRay
{
    glm::vec3 m_start;
    glm::vec3 m_dir;
};

struct SBlackHole
{
    glm::vec3 m_center;
    double m_rad;
    double m_mass;
};

struct SDisk
{
    glm::vec3 m_center;
    double m_rad;
};

struct SCamera
{
    glm::vec3 m_pos;          // Camera position and orientation
    glm::vec3 m_forward;      // Orthonormal basis
    glm::vec3 m_right;
    glm::vec3 m_up;

    glm::vec2 m_viewAngle;    // View angles, rad
    glm::uvec2 m_resolution;  // Image resolution: w, h

    std::vector<glm::vec3> m_pixels;  // Pixel array
};

struct SMesh
{
    std::vector<glm::vec3> m_vertices;  // vertex positions
    std::vector<glm::uvec3> m_triangles;  // vetrex indices
};

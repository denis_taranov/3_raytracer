#pragma once

#include "Types.h"

class CScene
{
public:
    void init(int xResFromFile, int yResFromFile, double blackHoleMassFromFile, double koefFromFile);
//private:
    SBlackHole black_hole;
    SDisk disk;
};

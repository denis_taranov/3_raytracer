#pragma once

#include "glm.hpp"
#include "Types.h"
#include "Scene.h"
#include "FreeImagePlus.h"

#include "string"
#include <cmath>
#include <iostream>
#include <vector>

using namespace glm;
using namespace std;

vec3 background[4096][4096], disk[4096][4096];

class CTracer
{
public:
    SRay MakeRay(glm::uvec2 pixelPos);  // Create ray for specified pixel
    glm::vec3 TraceRay(SRay ray); // Trace ray, compute its color
    void RenderImage(int xRes, int yRes);
    void SaveImageToFile(std::string fileName);
    fipImage* LoadImageFromFile(std::string fileName);
    void setCameraPosition(glm::vec2 matrix_res, glm::vec3 pos, int angle);
public:
    SCamera m_camera;
    CScene* m_pScene;
    /*fipImage *background, *disk;*/
};
